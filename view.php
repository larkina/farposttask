<?php


// dat --- json данные
function printCities($dat) {
    $data = json_decode($dat, true);
    
    $cities = $data["data"]["cities"];
    $cities_count = $data["data"]["total"];
    
    foreach ($cities as $key => $value) {
        echo "<a href=getFilms.php?id="
            .$value["id"].">"
            .$value["name"]."</p>";
    }
}

function printFilmList($dat) {
    date_default_timezone_set("Asia/Vladivostok");
    
    $data = json_decode($dat, true);
   
    $films = $data["data"]["films"];
    
    foreach($films as $key => $value) {
        $title = $value["title"];
        $img = $value["picture"];
        $popular = $value["popularity"];
        $genre = $value["genre"];
        $actors = $value["starring"];
        $director = $value["director"];
        $ar = $value["age_restrictions"];
        $seanse = $value["seances"];
        $id = $value["id"];
        $ss = "";
        
        $s_count = 0;
        foreach ($seanse as $key => $value) {
            $s_count++;
            $time = date("m:h ", $value["date_time"]);
            
            // Есть айди кинотетра, но нет справочника =(
            $theatre = $value["theatre_id"];
            
            $ss = $ss."$time  ";
            if ($s_count > 6)
                break;
        }
        
        echo <<<END
        <div class="hero-unit">
        
        <table border="0" >
        <tr>
        <td colspan=2><h3>$title</h3> <td>
        </tr>
        <tr>
        <td>
            
            <img class="img-circle" src="$img"> 
            <h5>☺ $popular</h5>
        </td>
        <td>
            <p> Режиссер: $director</p>
            <p> Жанр: $genre</p>
            <p> Актеры: $actors</p>   
            <p> Ограничения по возрасту: $ar</p>
        </td>
        </tr>
   
        </table>
        
           
       <p>
<a class="btn btn-info dropdown-toggle" href="filmInfo.php?id=$id&title=$title&ss=$ss">Подробнее о фильме »</a>
<div class="accordion">
<h3>Расписание</h3>
<div>
<p>$ss</p>
</div>
</div>
</button>                
                
</p>    
            
        </div>
END;
    }
}


function printFullFilmInfo($dat, $title, $ss)  {
   $data = json_decode($dat, true); 
   
   $film = $data["data"]["film"];
   $frames = $data["data"]["film"]["frame_list"];
   $commets = $data["data"]["comments"];
   
   $img = $film["picture_big"];
   $descp = $film["additional_description"];
   
   $fr = "";

  foreach($frames as $key => $value) {
      $src = $value["file"];
      $fr = $fr."<img src=\"".$src."\">";
  }
   
   echo <<<END
 
    
   
    <div class="hero-unit">
        
        <table border="0" >
        <tr>
        <td colspan=2><h3>$title</h3> <td>
        </tr>
        <tr>
        <td>
            
            <img class="img-circle" src="$img"> 
        </td>
        <td style="margin: 5px">
           <p>$descp</>
        </td>
        </tr>
   
        </table>
        
           
       <p>

<div class="accordion">
<h3>Расписание</h3>
<div>
<p>$ss</p>
</div>
</div>
</button>                
                
</p>    
            
        </div>
    
      <div class="hero-unit"> 
$fr
   </div>
           
END;
   
}
