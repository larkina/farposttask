<?php


// большой костыль
function json_fix_cyr($json_str) {
    
    $cyr_chars = array (
    '\u0430' => 'а', '\u0410' => 'А',
    '\u0431' => 'б', '\u0411' => 'Б',
    '\u0432' => 'в', '\u0412' => 'В',
    '\u0433' => 'г', '\u0413' => 'Г',
    '\u0434' => 'д', '\u0414' => 'Д',
    '\u0435' => 'е', '\u0415' => 'Е',
    '\u0451' => 'ё', '\u0401' => 'Ё',
    '\u0436' => 'ж', '\u0416' => 'Ж',
    '\u0437' => 'з', '\u0417' => 'З',
    '\u0438' => 'и', '\u0418' => 'И',
    '\u0439' => 'й', '\u0419' => 'Й',
    '\u043a' => 'к', '\u041a' => 'К',
    '\u043b' => 'л', '\u041b' => 'Л',
    '\u043c' => 'м', '\u041c' => 'М',
    '\u043d' => 'н', '\u041d' => 'Н',
    '\u043e' => 'о', '\u041e' => 'О',
    '\u043f' => 'п', '\u041f' => 'П',
    '\u0440' => 'р', '\u0420' => 'Р',
    '\u0441' => 'с', '\u0421' => 'С',
    '\u0442' => 'т', '\u0422' => 'Т',
    '\u0443' => 'у', '\u0423' => 'У',
    '\u0444' => 'ф', '\u0424' => 'Ф',
    '\u0445' => 'х', '\u0425' => 'Х',
    '\u0446' => 'ц', '\u0426' => 'Ц',
    '\u0447' => 'ч', '\u0427' => 'Ч',
    '\u0448' => 'ш', '\u0428' => 'Ш',
    '\u0449' => 'щ', '\u0429' => 'Щ',
    '\u044a' => 'ъ', '\u042a' => 'Ъ',
    '\u044b' => 'ы', '\u042b' => 'Ы',
    '\u044c' => 'ь', '\u042c' => 'Ь',
    '\u044d' => 'э', '\u042d' => 'Э',
    '\u044e' => 'ю', '\u042e' => 'Ю',
    '\u044f' => 'я', '\u042f' => 'Я',

    '\r' => '',
    '\n' => '<br />',
    '\t' => ''
    );

    foreach ($cyr_chars as $cyr_char_key => $cyr_char) {
    $json_str = str_replace($cyr_char_key, $cyr_char, $json_str);
    }
    return $json_str;
} 

function execQuery($method, $params) {
    
    $agent= 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    
    $baseUrl = 'http://kino.vl.ru/api/mobile/v1.0/get.json';
    
    $cnt = count($params);
    $paramStr = "";

    foreach ($params as $key => $value) {
        $paramStr = $paramStr."$key=$value&";
    }
    
    $url = "$baseUrl?method=$method&$paramStr";
    
   // echo "Url = $url";
    
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_USERAGENT, $agent);
    $result=curl_exec($ch);
    curl_close($ch); 
    
    if (!json_decode($result, true)["success"]) {
        echo "There was error with msg:".json_decode($result, true)["message"];
        return NULL;
    }
    
    return json_fix_cyr($result);
}

function getCities() {
    $res = execQuery("CityList", array());
    return $res;
}


function getActualCityList($cityId, $start, $end) {
    return execQuery("ActualFilmList", 
            array("cityId"=>$cityId, "start"=>$start, "end"=>$end));
}

function getFullFilmInfo($filmId) {
    return execQuery("FullFilmInfo", array("filmId"=>$filmId));
}

